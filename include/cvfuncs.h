#pragma once

#include "matrix.h"
#include "object.h"
#include <map>
#include <numeric>
#include <iterator>

namespace cv_funcs
{
    enum conv_type{
        RGB2YUV,
        YUV2RGB,
        RGB2GREY,
        GREY2RGB,
        RGB2HSV
    };
    using std::min;
    using std::max;
    #define to_uchar(x) uint(std::max(std::min(255.0, (x)), 0.0))

    vector <int> compute_hist(const GreyImage &mat)
    {
        vector<int>hist(256);
        for (uint y = 0; y < mat.n_rows; y++) {
            for (uint x = 0; x < mat.n_cols; x++) {
                ++hist[mat(y, x)];
            }
        }
        return hist;
    }

    vector <int> compute_hist(const Image &mat)
    {
        vector<int>hist(256);
        for (uint y = 0; y < mat.n_rows; y++) {
            for (uint x = 0; x < mat.n_cols; x++) {
                uint r, g, b;
                std::tie(r, g, b) = mat(y, x);
                ++hist[r];
            }
        }
        return hist;
    }

    void smooth_hist(vector <int> &hist)
    {
        double rad = 2;
        int sigma = ceil(rad / 3.0);
        vector <double> gauss(rad*2 + 1);
        const double pi = 3.1415926535;
        const double e = 2.71828182846;
        for (int i = 0; i < rad*2+1; i++) {
            gauss[i] = 1.0 / (sqrt(2 * pi) * sigma) * pow(e, -(i-rad)*(i-rad)/(2*sigma*sigma));
        }
        int start = rad;
        int end = hist.size() - rad;


        for (int i = start; i < end; ++i) {
            double tmp = 0;
            int t = 0;
            for (int j = i - rad; j < i + rad; ++j) {
                tmp += hist[j] * gauss[t++];
            }
            hist[i] = tmp;
        }

    }

    void convert(const Image &src, Image &dst, int mode=conv_type::RGB2YUV)
    {
        if (mode == conv_type::RGB2GREY) {
            throw std::string("Output image has to be 1-channeled in such a mode");
        }
        if (mode == conv_type::GREY2RGB) {
            throw std::string("Input image has to be 1-channeled in such a mode");
        }
        if (mode == conv_type::RGB2YUV) {
            for (uint y = 0; y < src.n_rows; y++) {
                for (uint x = 0; x < src.n_cols; x++) {
                    uint r, g, b, yc, uc, vc;
                    std::tie(r, g, b) = src(y, x);
                    yc = 0.299 * r + 0.587 * g + 0.114 * b;
                    uc = to_uchar(-0.168736 * r - 0.331264 * g + 0.5 * b + 128.0);
                    vc = to_uchar(0.5 * r - 0.418688 * g - 0.081312 * b + 128.0);
                    dst(y, x) = std::make_tuple(yc, uc, vc);
                }
            }
        }
        if (mode == conv_type::YUV2RGB) {
            for (uint y = 0; y < src.n_rows; y++) {
                for (uint x = 0; x < src.n_cols; x++) {
                    uint r, g, b, yc, uc, vc;
                    std::tie(yc, uc, vc) = src(y, x);
                    r = to_uchar(yc + 1.4075 * (vc - 128.0));
                    g = to_uchar(yc - 0.3455 * (uc - 128.0) - 0.7169 * (vc - 128.0));
                    b = to_uchar(yc + 1.7790 * (uc - 128.0));
                    dst(y, x) = std::make_tuple(r, g, b);
                }
            }
        }
    }

    void convert(const Image &src, GreyImage &dst, int mode=conv_type::RGB2GREY)
    {
        if (mode == conv_type::RGB2GREY) {
            for (uint y = 0; y < src.n_rows; y++) {
                for (uint x = 0; x < src.n_cols; x++) {
                    uint r, g, b, yc;
                    std::tie(r, g, b) = src(y, x);
                    yc = 0.299 * r + 0.587 * g + 0.114 * b;
                    dst(y, x) = yc;
                }
            }
        }
        if (mode == conv_type::RGB2HSV) {
            for (uint y = 0; y < src.n_rows; y++) {
                for (uint x = 0; x < src.n_cols; x++) {
                    uint r, g, b, s = 0;
                    std::tie(r, g, b) = src(y, x);
                    uint mx = std::max(r, std::max(g, b));
                    uint mn = std::min(r, std::min(g, b));
                    if (mx == mn) {
                        s = 0;
                    } else {
                        s = 255.0 - 255.0 * mn / mx;
                    }

                    dst(y, x) = s;
                }
            }
        }
    }

    void convert(const GreyImage &src, Image &dst, int mode=conv_type::GREY2RGB)
    {
        if (mode != conv_type::GREY2RGB) {
            throw std::string("Wrong images dimensions");
        }
        for (uint y = 0; y < src.n_rows; y++) {
            for (uint x = 0; x < src.n_cols; x++) {
                int yc = src(y, x);
                dst(y, x) = std::make_tuple(yc, yc, yc);
            }
        }
    }

    void binarize(const GreyImage &src, GreyImage &dst, int threshold=-1)
    {
        if (threshold == -1) {
            auto hist = compute_hist(src);
            smooth_hist(hist);
            auto h_max = max_element(hist.begin(), hist.end());
            int total = src.n_rows * src.n_cols;
            int h = std::distance(hist.begin(), h_max), sum = 0;
            while (h < 255 && 1.0 * sum / total < 0.20) { // beware magic number!
                sum += hist[h++];
            }
            threshold = h;
        }
        for (uint y = 0; y < src.n_rows; y++) {
            for (uint x = 0; x < src.n_cols; x++) {
                dst(y, x) = int(src(y, x)) >= threshold ? 255 : 0;
            }
        }
    }

    void adjust(const Image &src, Image &dst)
    {
        convert(src, dst, conv_type::RGB2YUV);
        vector <int> hist = compute_hist(src);
        int mn = 0, mx = 255, mean = 0;
        while (hist[mn] == 0) {
            ++mn;
        }
        while (hist[mx] == 0) {
            --mx;
        }
        for (uint y = 0; y < src.n_rows; y++) {
            for (uint x = 0; x < src.n_cols; x++) {
                uint yc, uc, vc;
                std::tie(yc, uc, vc) = src(y, x);
                dst(y, x) = std::make_tuple((yc - mn) * 255.0 / (mx - mn) + mean, uc, vc);
            }
        }
        convert(dst, dst, conv_type::YUV2RGB);
    }

    struct DSU
    {
        std::vector <uint> parent;
        std::vector <uint> size;
        std::vector <Rect> box;
        uint sets_num;
        uint n_cols;
        DSU(uint sz, uint n_cols_) : parent(), size(), box(), sets_num(0), n_cols(n_cols_) {
            parent.resize(sz);
            size.resize(sz);
            box.resize(sz);
        }
        void make_set(uint x) {
            parent[x] = x;
            size[x] = 1;
            box[x] = Rect(x % n_cols, x / n_cols, 1, 1);
            ++sets_num;
        }
        uint find_set(uint x) {
            if (parent[x] == x) {
                return x;
            }
            return parent[x] = find_set(parent[x]);
        }
        void merge_set(uint x, uint y) {
            uint p_x = find_set(x);
            uint p_y = find_set(y);
            if (p_x == p_y) {
                return;
            }
            uint x2 = box[p_y].x, y2 = box[p_y].y, w2 = box[p_y].w, h2 = box[p_y].h;
            uint x1 = box[p_x].x, y1 = box[p_x].y, w1 = box[p_x].w, h1 = box[p_x].h;
            uint w = 0, h = 0, xr = 0, yr = 0;
            xr = min(x1, x2);
            yr = min(y1, y2);
            if (x1 + w1 > x2 + w2) {
                w = x1 + w1 - xr;
            } else {
                w = x2 + w2 - xr;
            }
            if (y1 + h1 > y2 + h2) {
                h = y1 + h1 - yr;
            } else {
                h = y2 + h2 - yr;
            }
            if (size[p_x] < size[p_y]) {
                size[p_y] += size[p_x];
                box[p_y] = Rect(xr, yr, w, h);
                parent[p_x] = p_y;
            } else {
                size[p_x] += size[p_y];
                box[p_x] = Rect(xr, yr, w, h);
                parent[p_y] = p_x;
            }
            --sets_num;
        }
    };

    std::vector <Object> get_objects(const GreyImage &img)
    {
        DSU sets(img.n_cols * img.n_rows, img.n_cols);
        for (uint y = 0; y < img.n_rows; y++) {
            for (uint x = 0; x < img.n_cols; x++) {
                uint num = y * img.n_cols + x;
                if (img(y, x) == 0) {
                    continue;
                }
                sets.make_set(num);
                if (y != 0 && img(y, x) == img(y - 1, x)) {
                    sets.merge_set((y - 1) * img.n_cols + x, num);
                }
                if (x != 0 && img(y, x) == img(y, x - 1)) {
                    sets.merge_set(y * img.n_cols + x - 1, num);
                }
            }
        }
        // extract set information from dsu
        std::map <uint, uint> ind;
        std::vector <Object> res;
        std::vector <uint> par;
        for (uint y = 0; y < img.n_rows; y++) {
            for (uint x = 0; x < img.n_cols; x++) {
                uint num = y * img.n_cols + x;
                if (img(y, x) == 0) {
                    continue;
                }
                uint pt = sets.find_set(num);
                if (ind.count(pt) == 0) {
                    ind[pt] = res.size();
                    res.push_back(Object(sets.box[pt], img.deep_submatrix(sets.box[pt])));
                    par.push_back(pt);
                }
            }
        }
        // clean all submatrixes
        int i = 0;
        for (auto obj : res) {
            uint y0 = obj.box.y, x0 = obj.box.x, w = obj.box.w, h = obj.box.h;
            for (uint y = y0; y < y0 + h; y++) {
                for (uint x = x0; x < x0 + w; x++) {
                    uint num = y * img.n_cols + x;
                    uint px = obj.img(y - y0, x - x0);
                    if (px == 255 && sets.find_set(num) != par[i]) {
                        obj.img(y - y0, x - x0) = 0;
                    }
                }
            }
            ++i;
        }
        res.erase(std::remove_if(res.begin(), res.end(), [](const Object &a) { return a.square() <= 30; }), res.end());
        return res;
    }

}
