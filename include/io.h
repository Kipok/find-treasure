#pragma once

#include "matrix.h"
#include "EasyBMP.h"


typedef Matrix<std::tuple<uint, uint, uint>> Image;
typedef Matrix<std::tuple<double, double, double>> dImage;
typedef Matrix<uint> GreyImage;
typedef Matrix<double> dGreyImage;

Image load_image(const char*);
void save_image(const Image&, const char*);
