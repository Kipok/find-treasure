
Object::Object(Rect box_, const GreyImage &img_)
    : img(img_), box(box_), orig(box_.y, box_.x)
{
}

uint Object::square() const
{
    uint sq = 0;
    for (uint y = 0; y < box.h; y++) {
        for (uint x = 0; x < box.w; x++) {
            sq += img(y, x) != 0;
        }
    }
    return sq;
}

Point Object::mass_centre() const
{
    uint sq = square();
    uint m_x = 0, m_y = 0;
    for (uint y = 0; y < box.h; y++) {
        for (uint x = 0; x < box.w; x++) {
            m_x += (img(y, x) != 0) * x;
            m_y += (img(y, x) != 0) * y;
        }
    }
    return Point(uint(m_y / sq + box.y), uint(m_x / sq + box.x));
}

uint Object::perimeter() const
{
    uint per = 0;
    GreyImage er = img.unary_map(ErosionOp());
    for (uint y = 0; y < box.h; y++) {
        for (uint x = 0; x < box.w; x++) {
            per += (img(y, x) - er(y, x)) != 0;
        }
    }
    return per;
}

double Object::compactness() const
{
    uint p = perimeter(), s = square();
    return 1.0 * p * p / s;
}

double Object::stat_moment(uint i, uint j) const
{
    Point mc = mass_centre();
    mc.x -= box.x;
    mc.y -= box.y;
    double res = 0;
    for (uint y = 0; y < box.h; y++) {
        for (uint x = 0; x < box.w; x++) {
            res += pow(1.0 * x - mc.x, i) * pow(1.0 * y - mc.y, j) * (img(y, x) != 0);
        }
    }
    return res;
}

double Object::elongation() const
{
    double m20 = stat_moment(2, 0);
    double m02 = stat_moment(0, 2);
    double m11 = stat_moment(1, 1);
    double num = m20 + m02 + sqrt((m20 - m02) * (m20 - m02) + 4 * m11 * m11);
    double denom = m20 + m02 - sqrt((m20 - m02) * (m20 - m02) + 4 * m11 * m11);
    return num / denom;
}

double Object::angle() const
{
    double m11 = stat_moment(1, 1);
    double m20 = stat_moment(2, 0);
    double m02 = stat_moment(0, 2);
    return 0.5 * atan2(2 * m11, m20 - m02);
}
