#pragma once

#include <cmath>
#include <algorithm>

typedef unsigned int uint;

struct Point {
    double y, x;
    Point() : y(0), x(0) {}
    Point(double y_, double x_) : y(y_), x(x_) {}
    Point operator + (const Point &p) const {
        return Point(y + p.y, x + p.x);
    }
    Point operator - (const Point &p) const {
        return Point(y - p.y, x - p.x);
    }
    bool operator ==(const Point &p) const {
        return fabs(y - p.y) < 1e-7 && fabs(x - p.x) < 1e-7;
    }
};

struct Rect {
    uint y, x;
    uint w, h;
    Rect() : y(0), x(0), w(0), h(0) {}
    Rect(uint x_, uint y_, uint w_, uint h_) : y(y_), x(x_), w(w_), h(h_) {}
};

struct Line {
    double a, b, c;
    Point p0, pn;
    Line(Point p1, Point p2) {
        a = p2.y - p1.y;
        b = p1.x - p2.x;
        c = p2.x * p1.y - p1.x * p2.y;
        p0 = p1;
        pn = p2;
    }
    Line(double y1, double x1, double y2, double x2) {
        a = y2 - y1;
        b = x1 - x2;
        c = x2 * y1 - x1 * y2;
        p0 = Point(y1, x1);
        pn = Point(y2, x2);
    }
    bool check_sign(double y1, double x1) const {
        return (y1 - p0.y) * (pn.y - p0.y) + (x1 - p0.x) * (pn.x - p0.x) >= 0;
    }
};

inline double len(double y1, double x1, double y2, double x2) {
    return sqrt((y1 - y2) * (y1 - y2) + (x1 - x2) * (x1 - x2));
}

inline double dist(const Line &l, const Rect &box)
{
    double ans = 1e9;
    double x1 = box.x;
    double y1 = box.y;
    double x2 = box.x + box.w - 1;
    double y2 = box.y + box.h - 1;
    double x0 = l.p0.x, y0 = l.p0.y;

    // check x1
    if (fabs(l.b) > 1e-7) {
        double x = x1, y = (-l.c - l.a * x1) / l.b;
        if (l.check_sign(y, x) && y1 <= y && y <= y2) {
            ans = std::min(ans, len(y0, x0, y, x));
        }
    }
    // check x2
    if (fabs(l.b) > 1e-7) {
        double x = x2, y = (-l.c - l.a * x2) / l.b;
        if (l.check_sign(y, x) && y1 <= y && y <= y2) {
            ans = std::min(ans, len(y0, x0, y, x));
        }
    }
    // check y1
    if (fabs(l.a) > 1e-7) {
        double y = y1, x = (-l.c - l.b * y1) / l.a;
        if (l.check_sign(y, x) && x1 <= x && x <= x2) {
            ans = std::min(ans, len(y0, x0, y, x));
        }
    }
    // check y2
    if (fabs(l.a) > 1e-7) {
        double y = y2, x = (-l.c - l.b * y2) / l.a;
        if (l.check_sign(y, x) && x1 <= x && x <= x2) {
            ans = std::min(ans, len(y0, x0, y, x));
        }
    }
    return ans;
}
