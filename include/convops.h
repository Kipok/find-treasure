#pragma once

#include "matrix.h"

class BoxFilterOp
{
public:
    tuple<uint, uint, uint> operator () (const Image &m) const
    {
        uint size = 2 * radius + 1;
        uint r, g, b, sum_r = 0, sum_g = 0, sum_b = 0;
        for (uint i = 0; i < size; ++i) {
            for (uint j = 0; j < size; ++j) {
                // Tie is useful for taking elements from tuple
                tie(r, g, b) = m(i, j);
                sum_r += r;
                sum_g += g;
                sum_b += b;
            }
        }
        auto norm = size * size;
        sum_r /= norm;
        sum_g /= norm;
        sum_b /= norm;
        return make_tuple(sum_r, sum_g, sum_b);
    }
    uint operator () (const GreyImage &m) const
    {
        uint size = 2 * radius + 1;
        uint sum = 0;
        for (uint i = 0; i < size; ++i) {
            for (uint j = 0; j < size; ++j) {
                sum += m(i, j);
            }
        }
        auto norm = size * size;
        sum /= norm;
        return sum;
    }
    // Radius of neighbourhoud, which is passed to that operator
    BoxFilterOp() : radius(1) {}
    BoxFilterOp(int rad) : radius(rad) {}
    const int radius;
};

class DilationOp // maximum, works with GreyImage
{
public:
    const int radius;
    DilationOp() : radius(1) {}
    DilationOp(int rad) : radius(rad) {}
    uint operator () (const GreyImage &mat) const
    {
        uint size = 2 * radius + 1;
        uint res = 0;
        for (uint i = 0; i < size; ++i) {
            for (uint j = 0; j < size; ++j) {
                res |= mat(i, j);
            }
        }
        return res;
    }
};

class ErosionOp // minimum, works with GreyImage
{
public:
    const int radius;
    ErosionOp() : radius(1) {}
    ErosionOp(int rad) : radius(rad) {}
    uint operator () (const GreyImage &mat) const
    {
        uint size = 2 * radius + 1;
        uint res = 255;
        for (uint i = 0; i < size; ++i) {
            for (uint j = 0; j < size; ++j) {
                res &= mat(i, j);
            }
        }
        return res;
    }
};

class MedianOp
{
public:
    const int radius;
    MedianOp() : radius(1) {}
    MedianOp(int rad) : radius(rad) {}
    tuple<uint, uint, uint> operator () (const Image &m) const
    {
        uint size = 2 * radius + 1;
        uint r, g, b;
        vector <uint> for_r, for_g, for_b;
        for (uint i = 0; i < size; ++i) {
            for (uint j = 0; j < size; ++j) {
                // Tie is useful for taking elements from tuple
                tie(r, g, b) = m(i, j);
                for_r.push_back(r);
                for_g.push_back(g);
                for_b.push_back(b);
            }
        }
        sort(for_r.begin(), for_r.end());
        sort(for_g.begin(), for_g.end());
        sort(for_b.begin(), for_b.end());
        r = for_r[for_r.size() / 2];
        g = for_g[for_g.size() / 2];
        b = for_b[for_b.size() / 2];
        return make_tuple(r, g, b);
    }
};

class GaussOp
{
public:
    const int radius;
    GaussOp() : radius(1) {}
    GaussOp(int rad) : radius(rad) {}
    uint operator () (const GreyImage &m) const
    {
        int size = 2 * radius + 1;
        double px = 0;
        int sigma = ceil(radius / 3.0);

        vector <vector<double>> gauss(size);

        const double pi = 3.1415926535;
        const double e = 2.71828182846;
        for (int i = 0; i < size; i++) {
            gauss[i].resize(size);
            for (int j = 0; j < size; j++) {
                double nom = -((i-radius)*(i-radius)+(j-radius)*(j-radius));
                gauss[i][j] = 1.0 / (2 * pi * sigma * sigma) * pow(e, nom/(2*sigma*sigma));
            }
        }
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                px += m(i, j) * gauss[i][j];

            }
        }

        return px;
    }
};
