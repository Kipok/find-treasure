#pragma once

#include "matrix.h"
#include "convops.h"

class Object
{
public:
    GreyImage img; // submatrix of source
    Rect box;
    Point orig;
    Object(Rect box_, const GreyImage &img_);
    uint square() const;
    Point mass_centre() const;
    uint perimeter() const;
    double compactness() const;
    double stat_moment(uint i, uint j) const;
    double elongation() const;
    double angle() const;
    bool operator ==(const Object &obj) {
        return obj.box.x == box.x && obj.box.y == box.y && obj.box.w == box.w && obj.box.h == box.h;
    }
};

#include "object.hpp"
