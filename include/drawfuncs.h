#pragma once

#include "matrix.h"

namespace cv_funcs
{
    void draw_rectangle(Image &src, const std::tuple<uint, uint, uint> &color, const Rect &box)
    {
        for (uint i = box.x; i < box.x + box.w; i++) {
            src(box.y, i) = src(box.y + box.h - 1, i) = color;
        }
        for (uint j = box.y; j < box.y + box.h; j++) {
            src(j, box.x) = src(j, box.x + box.w - 1) = color;
        }
    }
    void draw_line(Image &src, const std::tuple<uint, uint, uint> &color, Point p1, Point p2)
    {
        if (p1 == p2) {
            return;
        }
        if (p1.x < 0 || uint(p1.x) >= src.n_cols || p1.y < 0 || uint(p1.y) >= src.n_rows) {
            throw std::string("Can't draw line: out of range");
        }
        if (p2.x < 0 || uint(p2.x) >= src.n_cols || p2.y < 0 || uint(p2.y) >= src.n_rows) {
            throw std::string("Can't draw line: out of range");
        }
        if (fabs(p1.x - p2.x) > fabs(p1.y - p2.y)) {
            if (p1.x > p2.x) {
                std::swap(p1, p2);
            }
            for (int x = p1.x; x <= p2.x; x++) {
                int y = (x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x) + p1.y;
                src(y, x) = color;
            }
        } else {
            if (p1.y > p2.y) {
                std::swap(p1, p2);
            }
            for (int y = p1.y; y <= p2.y; y++) {
                int x = (y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y) + p1.x;
                src(y, x) = color;
            }
        }
    }
    void draw_line(Image &src, const std::tuple<uint, uint, uint> &color, uint y1, uint x1, uint y2, uint x2)
    {
        draw_line(src, color, Point(y1, x1), Point(y2, x2));
    }
}
