#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <initializer_list>
#include <limits>
#include <vector>
#include <tuple>
#include <iterator>
#include <algorithm>

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::ofstream;
using std::vector;
using std::tuple;
using std::tie;
using std::make_tuple;

#include "io.h"
#include "matrix.h"
#include "cvfuncs.h"
#include "drawfuncs.h"
#include "convops.h"

using namespace cv_funcs;

int get_midcolor(const Object &obj, const Image &img)
{
    int r = 0, g = 0, b = 0, rc = 0, bc = 0, gc = 0;
    for (uint y = obj.box.y; y < obj.box.y + obj.box.h; y++) {
        for (uint x = obj.box.x; x < obj.box.x + obj.box.w; x++) {
            std::tie(r, g, b) = img(y, x);
            rc += r;
            gc += g;
            bc += b;
        }
    }
    int sq = obj.square();
    return (rc - gc - bc) / sq;
}

Object get_redarrow(std::vector<Object> objects, const Image &img, double &etal)
{
    sort(objects.begin(), objects.end(), [](const Object &a, const Object &b) { return a.elongation() < b.elongation();});
    double med = objects[objects.size() / 2].elongation();
    etal = med;
    std::vector<Object> res;
    int mx = -255, mxi = 0, i = 0;
    for (auto obj : objects) {
        if (fabs(obj.elongation() - med) < 0.5) { // beware magic number!
            auto px = get_midcolor(obj, img);
            if (mx < px) {
                mxi = i;
                mx = px;
            }
        }
        ++i;
    }
    return objects[mxi];
}

int compute_metric(Point p, double angle, const Object &obj, const Image &img)
{
//    int r, g, b;
//    std::tie(r, g, b) = img(p.y, p.x);
//    return g - r - b;
    int ans = 0;
    double cs = -sin(angle), sn = cos(angle);
    Point mc = p;
    Line l(mc, mc + Point(sn, cs));
    if (fabs(cs) > fabs(sn)) {
        uint x = mc.x;
        while (x < obj.box.x + obj.box.w) {
            int y = (-l.c - l.a * x) / l.b;
            if (uint(y) >= obj.box.y + obj.box.h || uint(y) < obj.box.y) {
                break;
            }
            ans += (obj.img(y - obj.box.y, x - obj.box.x) != 0);
            ++x;
        }
    } else {
        uint y = mc.y;
        while (y < obj.box.y + obj.box.h) {
            int x = (-l.c - l.b * y) / l.a;
            if (uint(x) >= obj.box.x + obj.box.w || uint(x) < obj.box.x) {
                break;
            }
            ans += (obj.img(y - obj.box.y, x - obj.box.x) != 0);
            ++y;
        }
    }
    if (fabs(cs) > fabs(sn)) {
        uint x = mc.x;
        while (x > obj.box.x) {
            int y = (-l.c - l.a * x) / l.b;
            if (uint(y) >= obj.box.y + obj.box.h || uint(y) < obj.box.y) {
                break;
            }
            ans += (obj.img(y - obj.box.y, x - obj.box.x) != 0);
            --x;
        }
    } else {
        uint y = mc.y;
        while (y > obj.box.y) {
            int x = (-l.c - l.b * y) / l.a;
            if (uint(x) >= obj.box.x + obj.box.w || uint(x) < obj.box.x) {
                break;
            }
            ans += (obj.img(y - obj.box.y, x - obj.box.x) != 0);
            --y;
        }
    }
    return ans;
}

Point get_direction(Point mc, double angle, const Object &obj, const Image &img)
{
    int met1 = -1e8, met2 = -1e8;
    double cs = cos(angle), sn = sin(angle);
    Line l(mc, mc + Point(sn, cs));
    if (fabs(cs) > fabs(sn)) {
        uint x = mc.x;
        while (x < obj.box.x + obj.box.w) {
            int y = (-l.c - l.a * x) / l.b;
            if (uint(y) >= obj.box.y + obj.box.h || uint(y) < obj.box.y) {
                break;
            }
            met1 = std::max(met1, compute_metric(Point(y, x), angle, obj, img));
            ++x;
        }
    } else {
        uint y = mc.y;
        while (y < obj.box.y + obj.box.h) {
            int x = (-l.c - l.b * y) / l.a;
            if (uint(x) >= obj.box.x + obj.box.w || uint(x) < obj.box.x) {
                break;
            }
            met1 = std::max(met1, compute_metric(Point(y, x), angle, obj, img));
            ++y;
        }
    }
    if (fabs(cs) > fabs(sn)) {
        uint x = mc.x;
        while (x > obj.box.x) {
            int y = (-l.c - l.a * x) / l.b;
            if (uint(y) >= obj.box.y + obj.box.h || uint(y) < obj.box.y) {
                break;
            }
            met2 = std::max(met2, compute_metric(Point(y, x), angle, obj, img));
            --x;
        }
    } else {
        uint y = mc.y;
        while (y > obj.box.y) {
            int x = (-l.c - l.b * y) / l.a;
            if (uint(x) >= obj.box.x + obj.box.w || uint(x) < obj.box.x) {
                break;
            }
            met2 = std::max(met2, compute_metric(Point(y, x), angle, obj, img));
            --y;
        }
    }
    if (met1 > met2) {
        if (fabs(cs) > fabs(sn)) {
            int x = mc.x + 20;
            int y = (-l.c - l.a * x) / l.b;
            return Point(y, x);
        } else {
            int y = mc.y + 20;
            int x = (-l.c - l.b * y) / l.a;
            return Point(y, x);
        }
    } else {
        if (fabs(cs) > fabs(sn)) {
            int x = mc.x - 20;
            int y = (-l.c - l.a * x) / l.b;
            return Point(y, x);
        } else {
            int y = mc.y - 20;
            int x = (-l.c - l.b * y) / l.a;
            return Point(y, x);
        }
    }
}

int check_obj(const std::vector<Object> objects, const Object &cur, Point p)
{
    int i = 0;
    for (auto obj : objects) {
        if (obj == cur) {
            ++i;
            continue;
        }
        if (p.x >= obj.box.x && p.x < obj.box.x + obj.box.w) {
            if (p.y >= obj.box.y && p.y < obj.box.y + obj.box.h) {
                return i;
            }
        }
        ++i;
    }
    return -1;
}

Object get_next(const Object &cur, const std::vector<Object> &objects, const Image &img)
{
    // cur has to be arrow! Otherwise nothing will work
    Point mc = cur.mass_centre();
    Point mn = get_direction(mc, cur.angle(), cur, img);
    Line l(mc, mn);
    if (fabs(l.p0.x - l.pn.x) > fabs(l.p0.y - l.pn.y)) {
        uint x = mc.x;
        while (0 < x && x < img.n_cols) {
            int y = (-l.c - l.a * x) / l.b;
            if (uint(y) >= img.n_rows || y < 0) {
                break;
            }
            int i = check_obj(objects, cur, Point(y, x));
            if (i != -1) {
                return objects[i];
            }
            //img(y, x) = std::make_tuple(0, 0, 255);
            x += l.pn.x - l.p0.x > 0 ? 1 : -1;
        }
    } else {
        uint y = mc.y;
        while (0 < y && y < img.n_rows) {
            int x = (-l.c - l.b * y) / l.a;
            if (uint(x) >= img.n_cols || x < 0) {
                break;
            }
            int i = check_obj(objects, cur, Point(y, x));
            if (i != -1) {
                return objects[i];
            }
            y += l.pn.y - l.p0.y > 0 ? 1 : -1;
        }
    }
    return objects[0];
}

bool isArrow(const Object &obj, double etal)
{
    return fabs(obj.elongation() - etal) < 0.5;
}

void pre_process(Image &img)
{
//    adjust(img, img);
    img = img.unary_map(MedianOp(2));
}

void post_process(GreyImage &img)
{
    img = img.unary_map(ErosionOp(1));
    img = img.unary_map(DilationOp(1));
}

tuple<vector<Rect>, Image>
find_treasure(Image& in)
{
    auto img = in.deep_copy();
    auto path = vector<Rect>();
    pre_process(img);
    GreyImage g_img(img.n_rows, img.n_cols);
    convert(img, g_img);
    binarize(g_img, g_img);
    post_process(g_img);
    auto objects = get_objects(g_img);
    double etal = 0;
    auto start = get_redarrow(objects, img, etal);
    Object next = get_next(start, objects, img);
    path.push_back(start.box);
    int i = 0;
    vector <Point> to_draw;
    to_draw.push_back(start.mass_centre());
    while (isArrow(next, etal)) {
        path.push_back(next.box);
        to_draw.push_back(next.mass_centre());
        start = next;
        next = get_next(start, objects, img);
        if (i++ > 100) {
            break;
        }
    }
    path.push_back(next.box);
    to_draw.push_back(next.mass_centre());
    for (uint j = 0; j < to_draw.size() - 1; j++) {
        draw_line(in, std::make_tuple(0, 255, 0), to_draw[j], to_draw[j + 1]);
    }
    draw_rectangle(in, std::make_tuple(255, 0, 0), next.box);
    return make_tuple(path, in);
}

tuple<vector<Rect>, Image>
test(Image& img)
{
    pre_process(img);
    GreyImage g_img(img.n_rows, img.n_cols);
    convert(img, g_img);
    binarize(g_img, g_img);
    post_process(g_img);
    convert(g_img, img);
    return make_tuple(vector<Rect>(), img);
}

int main(int argc, char **argv)
{
    if (argc != 4)
    {
        cout << "Usage: " << endl << argv[0]
             << " <in_image.bmp> <out_image.bmp> <out_path.txt>" << endl;
        return 0;
    }

    try {
        Image src_image = load_image(argv[1]);
        ofstream fout(argv[3]);

        vector<Rect> path;
        Image dst_image;
        tie(path, dst_image) = /*test(src_image);*/find_treasure(src_image);
        save_image(dst_image, argv[2]);

        for (const auto &obj : path)
        {
            fout << obj.x << " " << obj.y << " " << obj.w << " " << obj.h << endl;
        }

    } catch (const string &s) {
        cerr << "Error: " << s << endl;
        return 1;
    }
}
