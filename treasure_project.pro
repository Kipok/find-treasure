TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    src/io.cpp \
    src/main.cpp \
    src/matrix_example.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    include/io.h \
    include/matrix.h \
    include/matrix.hpp \
    include/cvfuncs.h \
    include/drawfuncs.h \
    src/object.h \
    include/object.h \
    include/object.hpp \
    include/convops.h \
    include/geomstructs.h

