import subprocess
import os

# specify all directories
img_dir = 'pic'
program = 'build/bin/main.exe'
out_dir = 'results'
cygwin_path = 'D:/cygwin/bin'
check_path = 'compare.py'
ans_path = 'labelling'

# add cygwin directory to path
os.environ["PATH"] += os.pathsep + cygwin_path

images = [ img_dir + '/' + f for f in os.listdir(img_dir) if os.path.isfile(os.path.join(img_dir,f)) ]

for img in images:
	print('Running {}..'.format(img))
	out_img_name = out_dir + '/' + os.path.split(img)[1]
	out_txt_name = os.path.splitext(out_img_name)[0] + '.txt'
	res_txt_name = ans_path + '/' + os.path.split(out_txt_name)[1]
	subprocess.call([program, img, out_img_name, out_txt_name])
	subprocess.call(['python.exe', os.path.abspath(check_path), res_txt_name, out_txt_name])
print('Testing completed')